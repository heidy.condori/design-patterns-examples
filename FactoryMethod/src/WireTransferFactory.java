
public class WireTransferFactory extends PaymentCreator {

	@Override
	public iPayment create() {
		return new WireTransfer();
	}

}
