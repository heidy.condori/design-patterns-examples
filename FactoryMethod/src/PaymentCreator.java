
public abstract class PaymentCreator {
	abstract public iPayment create();

	public String type() {
		iPayment payment = this.create();
		return "PayFactory " + payment.payType();
	}
	
	public String amount() {
		iPayment payment = this.create();
		return "amount: " + payment.amount();
	}
}
