
public class CreditCard implements iPayment {

	@Override
	public String payType() {
		return "CreditCard";
	}

	@Override
	public String amount() {
		return "20 Bs.";
	}

}
