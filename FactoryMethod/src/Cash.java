
public class Cash implements iPayment {

	@Override
	public String payType() {
		return "Cash";
	}

	@Override
	public String amount() {
		return "60 Bs.";
	}

}
