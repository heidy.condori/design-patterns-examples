
public class CashFactory extends PaymentCreator {

	@Override
	public iPayment create() {
		return new Cash();
	}

}
