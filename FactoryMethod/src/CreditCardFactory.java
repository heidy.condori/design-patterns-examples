
public class CreditCardFactory extends PaymentCreator {

	@Override
	public iPayment create() {
		return new CreditCard();
	}

}
