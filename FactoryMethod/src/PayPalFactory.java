
public class PayPalFactory extends PaymentCreator {

	@Override
	public iPayment create() {
		return new Paypal();
	}

}
