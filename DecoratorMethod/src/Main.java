import java.util.Scanner;

public class Main {

	private static void showProduct(int option, IProduct product){
		switch(option) {
			case 5:
			System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+ PRODUCT +-+-+-+-+-+-+-+-+-+-+-+-+");
			System.out.println("NAME: " + product.getName());
			System.out.println("DESCRIPTION: " + product.getDescription());
			System.out.println("TOTAL PRICE: " + product.getPrice());
			System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
			break;
		}
	}
	private static IProduct addProduct(int option, IProduct product){
		switch(option) {
			case 1:
				product = new LaptopCaseDecorator(product);
				break;
			case 2:
				product = new PowerBankDecorator(product);
				break;
			case 3:
				product = new MouseWirelessDecorator(product);
				break;
			case 4:
				product = new MouseGamerDecorator(product);
				break;
		}
		return product;
	}

	private static void menu(Scanner s, IProduct product){
		int option;
		do{
			System.out.println("Choose a product to add: ");
			System.out.println("1.- Laptop Case ");
			System.out.println("2.- PowerBank ");
			System.out.println("3.- Mouse Wireless ");
			System.out.println("4.- Mouse Gamer ");
			System.out.println("5.- see price ");
			System.out.println("0.- Exit ");
			option = s.nextInt();
			product = addProduct(option,product);
			showProduct(option,product);
		}while(option != 0);
	}

	public static void main(String[] args) {
		IProduct laptopDell = new LaptopDell();
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+ LAPTOP +-+-+-+-+-+-+-+-+-+-+-+-+");
		System.out.println("NAME: " + laptopDell.getName());
		System.out.println("DESCRIPTION: " + laptopDell.getDescription());
		System.out.println("TOTAL PRICE: " + laptopDell.getPrice());
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
		
		IProduct powerBank = new PowerBankDecorator(laptopDell);
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+ ADDING POWERBANK  +-+-+-+-+-+-+-+-+-+-+-+-+");
		System.out.println("NAME: " + powerBank.getName());
		System.out.println("DESCRIPTION: " + powerBank.getDescription());
		System.out.println("TOTAL PRICE: " + powerBank.getPrice());
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
		
		IProduct mouse = new MouseGamerDecorator(powerBank);
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+ ADDING mouse  +-+-+-+-+-+-+-+-+-+-+-+-+");
		System.out.println("NAME: " + mouse.getName());
		System.out.println("DESCRIPTION: " + mouse.getDescription());
		System.out.println("TOTAL PRICE: " + mouse.getPrice());
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");

		IProduct laptopAsus = new LaptopAsus();
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+ LAPTOP  +-+-+-+-+-+-+-+-+-+-+-+-+");
		System.out.println("NAME: " + laptopAsus.getName());
		System.out.println("DESCRIPTION: " + laptopAsus.getDescription());
		System.out.println("TOTAL PRICE: " + laptopAsus.getPrice());
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");

		IProduct mouseGamer = new MouseGamerDecorator(laptopAsus);
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+ ADDING mouse  +-+-+-+-+-+-+-+-+-+-+-+-+");
		System.out.println("NAME: " + mouseGamer.getName());
		System.out.println("DESCRIPTION: " + mouseGamer.getDescription());
		System.out.println("TOTAL PRICE: " + mouseGamer.getPrice());
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");




		Scanner s = new Scanner(System.in);
		String  name;
		String  description;
		float  price;
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+ Introduce a new Product  +-+-+-+-+-+-+-+-+-+-+-+-+");
		System.out.println("NAME: ");
		name = s.nextLine();
		System.out.println("DESCRIPTION: ");
		description = s.nextLine();
		System.out.println("TOTAL PRICE: " );
		price = s.nextFloat();
		IProduct product = new Product(name,description,price);
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
		menu(s,product);




	}

}
