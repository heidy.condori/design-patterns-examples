
public class MouseGamerDecorator extends ProductDecorator {
	public MouseGamerDecorator(IProduct product) {
		super(product);
	}

	@Override
	public String getName() {
		return getProduct().getName();
	}

	@Override
	public String getDescription() {
		return getProduct().getDescription() + " + optic sensor 5000pdi";
	}

	@Override
	public float getPrice() {
		return (float) (getProduct().getPrice() + (50.5));
	}

}
