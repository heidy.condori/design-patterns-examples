
public abstract class ProductDecorator implements IProduct {
	private IProduct product;

	public ProductDecorator(IProduct product) {
		setProduct(product);
	}

	public IProduct getProduct() {
		return product;
	}

	public void setProduct(IProduct product) {
		this.product = product;
	}
}
