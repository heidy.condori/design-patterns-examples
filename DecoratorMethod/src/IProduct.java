
public interface IProduct {
	String getName();
	String getDescription();

	float getPrice();
}
