
public class PowerBankDecorator extends ProductDecorator {

	public PowerBankDecorator(IProduct product) {
		super(product);
	}

	@Override
	public String getName() {
		return getProduct().getName();
	}

	@Override
	public String getDescription() {
		return getProduct().getDescription() + " + PowerBank 4200mHA";
	}

	@Override
	public float getPrice() {
		return (float) (getProduct().getPrice() + (90));
	}

}
