
public class LaptopDell extends Laptop {

	@Override
	public String getName() { return "DELL"; }

	public String getDescription() {
		return "Laptop Gamer Core i7 9th generation, 12GB of RAM ";
	}

	public float getPrice() {
		return 9860;
	}

}
