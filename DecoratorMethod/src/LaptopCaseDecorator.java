
public class LaptopCaseDecorator extends ProductDecorator {

	public LaptopCaseDecorator(IProduct product) {
		super(product);
	}

	@Override
	public String getName() {
		return getProduct().getName();
	}

	@Override
	public String getDescription() {
		return getProduct().getDescription() + " + LaptopCase of Leather";
	}

	@Override
	public float getPrice() {
		return (float) (getProduct().getPrice() + (42.5));
	}

}
