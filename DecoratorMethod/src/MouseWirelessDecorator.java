
public class MouseWirelessDecorator extends ProductDecorator {
	public MouseWirelessDecorator(IProduct product) {
		super(product);
	}

	@Override
	public String getName() {
		return getProduct().getName();
	}

	@Override
	public String getDescription() {
		return getProduct().getDescription() + " + Bluetooth 5.0";
	}

	@Override
	public float getPrice() {
		return (float) (getProduct().getPrice() + (20));
	}
}
