
public class LaptopAsus extends Laptop {

	@Override
	public String getName() { return "ASUS TUF Gamer"; }

	public String getDescription() {
		return "15.6´´  16GB of RAM, 512GB storage";
	}

	public float getPrice() {
		return 7999;
	}

}
