
public class Client {

	public static void main(String[] args) {
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-ONLINE ORDER+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-");
		BuyGiftTemplate onLineOrder = new OnlineOrder();
		onLineOrder.processOrder(true);
        System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-");
        System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+- STORE ORDER+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-");
        BuyGiftTemplate storeOrder = new StoreOrder();
        storeOrder.processOrder(false);
        System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-");

	}

}
