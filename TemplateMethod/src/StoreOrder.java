
public class StoreOrder extends BuyGiftTemplate
{
 
    @Override
    public void doSelect()
    {
        System.out.println("Customer chooses one or more gifts from shelf.");
    }
 
    @Override
    public void doPayment()
    {
        System.out.println("Customer pays at counter with cash/QR/POS");
    }

	@Override
	public void doWriteNote() {
		System.out.println("Customer write a message or note ");
		System.out.println("Customer add note at gift ");
		
	}

    @Override
    public void doDelivery()
    {
        System.out.println("Gift delivered to in delivery counter to Customer");
    }
}
