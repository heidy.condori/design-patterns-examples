
public abstract class BuyGiftTemplate {
	boolean isSmall;

	abstract void doSelect();

	abstract void doPayment();

	abstract void doWriteNote();

	abstract void doDelivery();

	public final void giftWrap() {
		System.out.println("Gift was wrap!!!");
	}

	public final void processOrder(boolean isSmall) {
		doSelect();
		doPayment();
		doWriteNote();
		if (isSmall) {
			giftWrap();
		}
		doDelivery();
	}
}
