
public class OnlineOrder extends BuyGiftTemplate {

	@Override
	public void doSelect() {
		System.out.println("Item added to online shopping cart");
		System.out.println("Get message or a note...");
		System.out.println("Get delivery address...");
	}

	@Override
	public void doPayment() {
		System.out.println("Online Payment through card or wire transfer...");

	}

	@Override
	public void doWriteNote() {
		System.out.println("Write message or a note...");

	}

	@Override
	public void doDelivery() {
		System.out.println("Ship the gift through employee to delivery address...");

	}

}
