import java.util.List;
import java.util.Random;

public class Developer implements IEmployee{
    private String name;

    public Developer(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void add(IEmployee employee) {
        System.out.println("This employee can't add");
    }

    @Override
    public void remove(IEmployee employee) {
        System.out.println("This employee can't remove");
    }

    @Override
    public List<IEmployee> getEmployees() {
        return null;
    }

    @Override
    public int calculatePoint() {
        return new Random().nextInt(24);
    }

    @Override
    public String toString() {
        return "Developer: " + name ;
    }
}
