import java.util.List;

public interface IEmployee {
    String getName();
    void add(IEmployee employee);
    void remove(IEmployee employee);
    List<IEmployee> getEmployees();
    int calculatePoint();
}
