public class ClientComposite {
    public static void  main(String... args){

        IEmployee manager1 = new Manager("Marcos");

        IEmployee developer1 = new Developer("Stephanie");
        IEmployee developer2 = new Developer("Pedro");
        IEmployee developer3 = new Developer("Carla");

        manager1.add(developer1);
        manager1.add(developer2);
        manager1.add(developer3);

        IEmployee manager2 = new Manager("Geraldine");

        IEmployee developer4 = new Developer("Allen");
        IEmployee developer5 = new Developer("Marta");

        manager2.add(developer4);
        manager2.add(developer5);

        System.out.println(manager1);
        System.out.println("Total Points: "+manager1.calculatePoint());


        System.out.println(manager1);
        System.out.println("Total Points: "+manager1.calculatePoint());


        System.out.println(manager2);
        System.out.println("Total Points: "+manager2.calculatePoint());

        manager2.add(manager1);
        System.out.println(manager2);
    }
}
