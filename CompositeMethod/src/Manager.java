import java.util.ArrayList;
import java.util.List;

public class Manager implements IEmployee {
    private List<IEmployee> employees;
    private String name;

    public Manager(String name) {
        this.name = name;
        this.employees = new ArrayList<>();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void add(IEmployee employee) {
        if (employee != null) {
            this.employees.add(employee);
        }
    }

    @Override
    public void remove(IEmployee employee) {
        if (employee != null) {
            this.employees.remove(employee);
        }
    }

    @Override
    public List<IEmployee> getEmployees() {
        return this.employees;
    }

    @Override
    public int calculatePoint() {
        if (this.employees.isEmpty()) {
            return 0;
        }
        return Math.round(this.employees.stream().mapToInt(employee -> {
            return employee.calculatePoint();
        }).sum());
    }

    @Override
    public String toString() {
        return "Manager: " + name + "\n"+
                "Employees=" + employees;
    }

}
