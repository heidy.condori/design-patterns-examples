public class Main {
    public static void main(String[] args) {
        ShoppingCart cart = new ShoppingCart();

        Product item1 = new Product("laptop ASUS",10000);
        Product item2 = new Product("powerBank", 100);
        Product item3 = new Product("mouse", 50);

        cart.addItem(item1);
        cart.addItem(item2);
        cart.addItem(item3);

        //pay by paypal
        IPaymentStrategy paypalMethod = new PaypalStrategy("wilson@email.com", "mypaswword");
        cart.pay(paypalMethod);

        //pay by paypal with 10% of discount
        IPaymentStrategy paypalWithDiscount = new PaypalDecorator(paypalMethod);
        cart.pay(paypalWithDiscount);

        //pay by credit card
        IPaymentStrategy creditCardlMethod = new CreditCardStrategy("Wilson Ramirez", "1234567890123456", "786", "12/15");
        cart.pay(creditCardlMethod);

        //pay by credit card with 5% of discount
        IPaymentStrategy creditCardlWithDiscount = new CreditCardDecorator(creditCardlMethod);
        cart.pay(creditCardlWithDiscount);

    }
}
