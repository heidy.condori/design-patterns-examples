public abstract class PaymentDecorator implements IPaymentStrategy {

    private IPaymentStrategy paymentStrategy;

    public PaymentDecorator(IPaymentStrategy paymentStrategy) {
        setDiscount(paymentStrategy);
    }

    public IPaymentStrategy getPaymentStrategy(){return paymentStrategy;}
    private void setDiscount(IPaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }
}
