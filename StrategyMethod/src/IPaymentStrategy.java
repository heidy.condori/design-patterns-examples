public interface IPaymentStrategy {
    double pay(double paymentAmount);
    String typeOfPayment();
}
