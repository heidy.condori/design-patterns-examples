public class PaypalDecorator extends PaymentDecorator{
    double PERCENTAGEOFDISCOUNT= 0.10;
    public PaypalDecorator(IPaymentStrategy paymentStrategy) {
        super(paymentStrategy);
    }

    @Override
    public double pay(double paymentAmount) {
        return getPaymentStrategy().pay(paymentAmount) - getPaymentStrategy().pay(paymentAmount)*PERCENTAGEOFDISCOUNT;
    }

    @Override
    public String typeOfPayment() {
        return getPaymentStrategy().typeOfPayment() +"with "+ PERCENTAGEOFDISCOUNT* 100 + " % of discount";
    }
}
