public class PaypalStrategy implements IPaymentStrategy {
    private String emailId;
    private String password;

    public PaypalStrategy(String emailId, String password) {
        this.emailId = emailId;
        this.password = password;
    }

    @Override
    public double pay(double paymentAmount) {
        return paymentAmount;
    }

    @Override
    public String typeOfPayment() {
        return " Paid using Paypal ";
    }

}
