import java.util.ArrayList;
import java.util.List;

public class ShoppingCart{
    List<Product> products;
    public ShoppingCart() {
        this.products = new ArrayList<Product>();
    }
    public void addItem(Product item){
        this.products.add(item);
    }

    public void removeItem(Product item){
        this.products.remove(item);
    }
    public double calculateTotal(){
        double sum = 0;
        for(Product product : products){
            sum+= product.getPrice();
        }
        return sum;
    }

    public void pay(IPaymentStrategy IPaymentStrategy){
        double totalPrice;
        totalPrice = calculateTotal();
        System.out.print(IPaymentStrategy.pay(totalPrice) + " Bs.");
        System.out.println(IPaymentStrategy.typeOfPayment());
    }
}
