
public class Bike implements iBike {
	private String frame;
	private String handles;
	private String brakes;
	private String wheel;
	private String pedals;
	private String saddle;

	@Override
	public void setFrame(String frame) {
		this.frame = frame;
		
	}

	@Override
	public void setHandles(String handles) {
		this.handles = handles;
		
	}

	@Override
	public void setBrakes(String brakes) {
		this.brakes = brakes;
		
	}

	@Override
	public void setWheel(String wheel) {
		this.wheel = wheel;
		
	}

	@Override
	public void setPedals(String pedals) {
		this.pedals = pedals;
		
	}

	@Override
	public void sedSaddle(String saddle) {
		this.saddle = saddle;
		
	}

}
