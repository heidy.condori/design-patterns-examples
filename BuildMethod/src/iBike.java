
public interface iBike {
	void setFrame(String frame);
	void setHandles(String handles);
	void setBrakes(String brakes);
	void setWheel(String wheel);
	void setPedals(String pedals);
	void sedSaddle(String saddle);
}
