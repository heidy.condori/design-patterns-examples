
public interface BikeBuilder {
	public void buidFrame();
	public void buidHandles();
	public void buidBrakes();
	public void buidWheel();
	public void buidPedals();
	public void buidSaddle();
}
