package Chair;


public interface iChair {
	String type();
	String color();
	int legsNumber();
}
