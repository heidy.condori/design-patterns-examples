package Table;

public class MetalTable implements iTable{

	@Override
	public String type() {
		return "Metal";
	}

	@Override
	public String color() {
		return "Gray...";
	}

	@Override
	public int legNumber() {
		return 4;
	}

}
