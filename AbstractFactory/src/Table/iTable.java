package Table;

public interface iTable {
	String type();

	String color();

	int legNumber();
}
