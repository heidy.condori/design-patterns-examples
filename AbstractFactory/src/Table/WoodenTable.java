package Table;

public class WoodenTable implements iTable{

	@Override
	public String type() {
		return "Wooden Table...";
	}

	@Override
	public String color() {
		return "Brown...";
	}

	@Override
	public int legNumber() {
		return 5;
	}

}
