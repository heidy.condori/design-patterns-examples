package Table;

public class PlasticTable implements iTable{

	@Override
	public String type() {
		return "Plastic Table";
	}

	@Override
	public String color() {
		return "Yellow";
	}

	@Override
	public int legNumber() {
		return 2;
	}

}
