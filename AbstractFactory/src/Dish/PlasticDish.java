package Dish;

public class PlasticDish implements iDish {

	@Override
	public String size() {
		return "Medium";
	}

	@Override
	public String color() {
		return "Green...";
	}

	@Override
	public String style() {
		return "Flowers Print...";
	}

}
