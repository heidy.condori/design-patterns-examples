package Dish;

public interface iDish {
	String size();

	String color();

	String style();
}
