package Dish;

public class MetalDish implements iDish {

	@Override
	public String size() {
		return "Small";
	}

	@Override
	public String color() {
		return "Gray...";
	}

	@Override
	public String style() {
		return "Animal Print";
	}

}
