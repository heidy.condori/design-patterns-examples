package Dish;

public class WoodenDish implements iDish {
	@Override
	public String size() {
		return "Big";
	}

	@Override
	public String color() {
		return "Green...";
	}

	@Override
	public String style() {
		return "Nothing...";
	}
}
