package Factory;

import Chair.PlasticChair;
import Chair.WoodenChair;
import Chair.iChair;
import Dish.WoodenDish;
import Dish.iDish;
import Table.WoodenTable;
import Table.iTable;

public class RestaurantWoodenFactory extends RestaurantFactory {

	@Override
	public iChair createChair() {
		iChair chair = new WoodenChair();
		return chair;
	}

	@Override
	public iTable createTable() {
		iTable table = new WoodenTable();
		return table;
	}

	@Override
	public iDish createDish() {
		iDish dish = new WoodenDish();
		return dish;
	}

}
