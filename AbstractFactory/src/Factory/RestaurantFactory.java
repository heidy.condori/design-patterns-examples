package Factory;

import Chair.iChair;
import Dish.iDish;
import Table.iTable;

public abstract class RestaurantFactory {
	public abstract iChair createChair();

	public abstract iTable createTable();

	public abstract iDish createDish();
}
