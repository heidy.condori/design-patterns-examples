package Factory;

import Chair.MetalChair;
import Chair.PlasticChair;
import Chair.iChair;
import Dish.MetalDish;
import Dish.PlasticDish;
import Dish.iDish;
import Table.PlasticTable;
import Table.iTable;

public class RestaurantPlasticFactory extends RestaurantFactory {

	@Override
	public iChair createChair() {
		iChair chair = new PlasticChair();
		return chair;
	}

	@Override
	public iTable createTable() {
		iTable table = new PlasticTable();
		return table;
	}

	@Override
	public iDish createDish() {
		iDish dish = new PlasticDish();
		return dish;
	}

}
