package Factory;

import Chair.MetalChair;
import Chair.iChair;
import Dish.MetalDish;
import Dish.iDish;
import Table.MetalTable;
import Table.iTable;

public class RestaurantMetalFactory extends RestaurantFactory {

	@Override
	public iChair createChair() {
		iChair chair = new MetalChair();
		return chair;
	}
	
	@Override
	public iTable createTable() {
		iTable table = new MetalTable();
		return table;
	}
	
	@Override
	public iDish createDish() {
		iDish dish = new MetalDish();
		return dish;
	}

}
