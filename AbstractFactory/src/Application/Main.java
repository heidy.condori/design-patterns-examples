package Application;

import java.net.SocketOptions;

import Chair.iChair;
import Dish.iDish;
import Factory.RestaurantFactory;
import Factory.RestaurantMetalFactory;
import Factory.RestaurantPlasticFactory;
import Factory.RestaurantWoodenFactory;
import Table.iTable;

public class Main {
	public static void main(String[] arg) {
		System.out.println("You can select different restaurants:");
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-PLASTIC-+-+-+-+-+-+-+-+-+-+-+-+-");
		RestaurantFactory plasticFactory = new RestaurantPlasticFactory();
		options(plasticFactory);
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-METAL-+-+-+-+-+-+-+-+-+-+-+-+-");
		RestaurantFactory metalFactory = new RestaurantMetalFactory();
		options(metalFactory);
		System.out.println("+-+-+-+-+-+-+-+-+-+-+-+-WOOD-+-+-+-+-+-+-+-+-+-+-+-+-");
		RestaurantFactory woodenFactory = new RestaurantWoodenFactory();
		options(woodenFactory);
	}
	
	public static void options(RestaurantFactory factory) {
		System.out.println("Type: [ ".concat(factory.createChair().type()).concat(" ] - Color: [ ").concat(factory.createChair().color()).concat(" ] - Legs: [ ").concat(String.valueOf(factory.createChair().legsNumber())).concat(" ]"));
		System.out.println("Type: [ ".concat(factory.createTable().type()).concat(" ] - Color: [ ").concat(factory.createTable().color()).concat(" ] - Legs: [ ").concat(String.valueOf(factory.createTable().legNumber())).concat(" ]"));
		System.out.println("Size: [ ".concat(factory.createDish().size()).concat(" ] - Color: [ ").concat(factory.createDish().color()).concat(" ] - Style: [ ").concat(factory.createDish().style()).concat(" ]"));
	}
}
